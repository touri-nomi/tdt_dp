public class SerialPrice extends PriceStrategy {
    public double getCharge(int daysRented) {
        double result = 2000;
        if (daysRented > 2)
            result += (daysRented - 2) * 1000;
        return result;
    }
}