
public abstract class PriceStrategy {
    public static final PriceStrategy CHILDRENS = new ChildrensPrice();
    public static final PriceStrategy REGULAR = new RegularPrice();
    public static final PriceStrategy NEW_RELEASE = new NewReleasePrice();
    public static final PriceStrategy SERIAL = new SerialPrice();

    abstract public double getCharge(int daysRented);

    public int getFrequentRenterPoints(int daysRented, PriceStrategy price) {
        if (daysRented > 1) {
            return 2;
        }

        if (price == PriceStrategy.NEW_RELEASE) {
            return 2;
        }
        return 1;
    }
}