import java.util.ArrayList;

public class StudentList {
    ISortStrategy sortStrategy;
    private ArrayList<Student> studentList;

    StudentList() {
        studentList = new ArrayList<Student>();
        sortStrategy = new SelectionSortStrategy();
    }

    public void addStudent(Student s) {
        studentList.add(s);
    }

    public Student removeStudent(Student s) {
        return studentList.remove(studentList.indexOf(s));
    }

    public void display() {
        for (Student s : studentList) {
            System.out.println(s);
        }
    }

    public void sort() {
        sortStrategy.sort(studentList.toArray(new Student[studentList.size()]), studentList.size());
    }

    public void setSortStrategy(ISortStrategy sortStrategy) {
        this.sortStrategy = sortStrategy;
    }
}
