public interface ISortStrategy {
    void sort(Comparable<Student>[] data, int count);
}