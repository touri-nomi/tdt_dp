import java.util.Date;

public class Student implements Comparable<Student> {

    private int code;
    private String name;
    private Date birthDate;

    Student(int code, String name, Date date) {
        this.code = code;
        this.name = name;
        this.birthDate = date;
    }

    @Override
    public int compareTo(Student o) {
        if (this.getCode() > o.getCode()) {
            return 1;
        }
        if (this.getCode() < o.getCode()) {
            return -1;
        }
        return 0;
    }

    /**
     * @return int return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Date return the birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate the birthDate to set
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

}