import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        StudentList students = new StudentList();

        students.addStudent(new Student(5, "Tung", new Date()));
        students.addStudent(new Student(2, "Phuc", new Date()));
        students.addStudent(new Student(2, "Phu", new Date()));
        students.addStudent(new Student(6, "Nhan", new Date()));
        students.addStudent(new Student(1, "Vinh", new Date()));

        students.setSortStrategy(new BubbleSortStrategy());

        students.display();
    }
}
