public class BubbleSortStrategy implements ISortStrategy {
    @Override
    public void sort(Comparable<Student>[] data, int count) {
        int n = data.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (data[j].compareTo((Student) data[j + 1]) == 1) {
                    // swap data[j+1] and data[j]
                    Comparable<Student> temp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temp;
                }
    }
}