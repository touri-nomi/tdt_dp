public class SelectionSortStrategy implements ISortStrategy {
    public void sort(Comparable<Student>[] data, int count) {
        int n = data.length;

        // One by one move boundary of unsorted subdataay
        for (int i = 0; i < n - 1; i++) {
            // Find the minimum element in unsorted dataay
            int min_idx = i;
            for (int j = i + 1; j < n; j++)
                if (data[j].compareTo((Student) data[min_idx]) == -1)
                    min_idx = j;

            // Swap the found minimum element with the first
            // element
            Comparable<Student> temp = data[min_idx];
            data[min_idx] = data[i];
            data[i] = temp;
        }
    }
}