public class InsertionSortStrategy implements ISortStrategy {
    @Override
    public void sort(Comparable<Student>[] data, int count) {
        int n = data.length;
        for (int i = 1; i < n; ++i) {
            Comparable<Student> key = data[i];
            int j = i - 1;

            /*
             * Move elements of data[0..i-1], that are
             * greater than key, to one position ahead
             * of their current position
             */
            while (j >= 0 && data[j].compareTo((Student) key) == 1) {
                data[j + 1] = data[j];
                j = j - 1;
            }
            data[j + 1] = key;
        }
    }
}