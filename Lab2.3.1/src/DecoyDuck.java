public class DecoyDuck extends Duck {
    public DecoyDuck() {
        // TODO7
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new MuteQuack());
    }

    public void display() {
        System.out.println("I'm a duck Decoy");
    }
}