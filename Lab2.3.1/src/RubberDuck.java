public class RubberDuck extends Duck {
    public RubberDuck() {
        // TODO6
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new Quack());
    }

    public void display() {
        System.out.println("I'm a rubber duckie");
    }
}