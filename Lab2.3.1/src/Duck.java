public abstract class Duck {
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public Duck() {
    }

    public void setFlyBehavior(FlyBehavior fb) {
        // TODO1
        this.flyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior qb) {
        // TODO2
        this.quackBehavior = qb;
    }

    abstract void display();

    public void performFly() {
        // TODO3
        this.flyBehavior.fly();
    }

    public void performQuack() {
        // TODO4
        this.quackBehavior.quack();
    }

    public void swim() {
        System.out.println("All ducks float, even decoys!");
    }
}