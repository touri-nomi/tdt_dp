public class DuckSimulator {
    public static void main(String[] args) {
        MarllardDuck mallard = new MarllardDuck();
        RubberDuck rubberDuckie = new RubberDuck();
        DecoyDuck decoy = new DecoyDuck();
        mallard.display();
        mallard.performQuack();
        mallard.performFly();
        rubberDuckie.display();
        rubberDuckie.performQuack();
        rubberDuckie.performFly();
        decoy.display();
        decoy.performQuack();
        decoy.performFly();
        // TODO8
        decoy.performFly();
    }
}